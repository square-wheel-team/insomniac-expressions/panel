window.addEventListener("DOMContentLoaded", init);

function init() {
  document.getElementById("expression_image_url").addEventListener('keyup', delay(reloadImage, 1000));
}


function delay(func, time_ms) {
  function wrapper() {
    const funcName = func.name || String(Date.now());
    window.timers = window.timers || {};
    clearTimeout(window.timers[funcName]);
    window.timers[funcName] = setTimeout(func, time_ms);
  }
  return wrapper;
}


function reloadImage() {
  const image = document.getElementById("expression-image");
  const imageUrl = document.getElementById("expression_image_url");
  image.src = imageUrl.value;
}
