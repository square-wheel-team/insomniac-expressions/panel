defmodule IEPanel.Expressions do
  @moduledoc """
  The Expressions context.
  """

  import Ecto.Query, warn: false
  alias IEPanel.Repo

  alias IEPanel.Expressions.Expression

  @doc """
  Returns the list of expressions.

  ## Examples

      iex> list_expressions()
      [%Expression{}, ...]

  """
  def list_expressions do
    Repo.all(Expression)
  end

  @doc """
  Gets a single expression.

  Raises `Ecto.NoResultsError` if the Expression does not exist.

  ## Examples

      iex> get_expression!(123)
      %Expression{}

      iex> get_expression!(456)
      ** (Ecto.NoResultsError)

  """
  def get_expression!(id), do: Repo.get!(Expression, id)

  @doc """
  Creates a expression.

  ## Examples

      iex> create_expression(%{field: value})
      {:ok, %Expression{}}

      iex> create_expression(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_expression(attrs \\ %{}) do
    %Expression{}
    |> Expression.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a expression.

  ## Examples

      iex> update_expression(expression, %{field: new_value})
      {:ok, %Expression{}}

      iex> update_expression(expression, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_expression(%Expression{} = expression, attrs) do
    expression
    |> Expression.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a expression.

  ## Examples

      iex> delete_expression(expression)
      {:ok, %Expression{}}

      iex> delete_expression(expression)
      {:error, %Ecto.Changeset{}}

  """
  def delete_expression(%Expression{} = expression) do
    Repo.delete(expression)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking expression changes.

  ## Examples

      iex> change_expression(expression)
      %Ecto.Changeset{data: %Expression{}}

  """
  def change_expression(%Expression{} = expression, attrs \\ %{}) do
    Expression.changeset(expression, attrs)
  end
end
