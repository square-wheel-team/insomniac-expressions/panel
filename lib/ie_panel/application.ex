defmodule IEPanel.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    mongo_conf = Application.get_env(:ie_panel, :mongo)
    children = [
      IEPanelWeb.Telemetry,
      {DNSCluster, query: Application.get_env(:ie_panel, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: IEPanel.PubSub},
      # Start a worker by calling: IEPanel.Worker.start_link(arg)
      # {IEPanel.Worker, arg},
      # Start to serve requests, typically the last entry
      IEPanelWeb.Endpoint,
      {
        Mongo,
        name: :mongo,
        username: mongo_conf[:username],
        password: mongo_conf[:password],
        auth_source: mongo_conf[:auth_source],
        ssl: mongo_conf[:ssl],
        ssl_opts: [
          ciphers: ['AES256-GCM-SHA384'], #also tried with ["AES256-GCM-SHA384"]
          versions: [:"tlsv1.2"],
          verify: :verify_none
        ],
        url: "#{mongo_conf[:protocol]}://#{mongo_conf[:host]}/#{mongo_conf[:database]}"
      }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: IEPanel.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    IEPanelWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
