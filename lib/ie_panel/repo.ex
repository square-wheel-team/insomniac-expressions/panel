defmodule IEPanel.Repo do
  def all(schema) do
    case Mongo.find(:mongo, schema.__schema__(:source), %{}) do
      %Mongo.Stream{docs: docs} -> Enum.map(docs, &set_loaded(struct(schema, convert_map_keys_to_atoms(&1))))
      {:error, _} -> []
    end
  end

  def get!(schema, id) do
    case Mongo.find_one(:mongo, schema.__schema__(:source), %{_id: parse_id(id)}) do
      %{"_id" => _} = doc -> set_loaded(struct(schema, convert_map_keys_to_atoms(doc)))
      nil -> raise Ecto.NoResultsError
    end
  end

  defp set_loaded(schema) do
    Ecto.put_meta(schema, state: :loaded)
  end

  def insert(%{errors: []} = changeset) do
    changeset = add_timestamp_changes(changeset)
    case Mongo.insert_one(:mongo, changeset.data.__meta__.source, Map.delete(changeset.changes, :_id)) do
      {:ok, %Mongo.InsertOneResult{inserted_id: id}} -> {:ok, struct(changeset.data, Map.put(changeset.changes, :_id, id))}
      {:error, _} -> {:error, changeset}
    end
  end

  def insert(%{errors: [_head | _tail]} = changeset) do
    {:error, changeset}
  end

  def delete(%{_id: id, __meta__: %{source: source}} = obj) do
    case Mongo.delete_one(:mongo, source, %{_id: parse_id(id)}) do
      {:ok, %Mongo.DeleteResult{deleted_count: 0}} -> {:error, :not_found}
      {:ok, %Mongo.DeleteResult{}} -> {:ok, obj}
      {:error, _} -> {:error, obj.__meta__.schema.changeset(obj, %{})}
    end
  end

  def update(%{valid?: true} = changeset) do
    changeset = add_timestamp_changes(changeset)
    IO.puts(inspect changeset.changes)
    case Mongo.find_one_and_update(:mongo, changeset.data.__meta__.source, %{_id: parse_id(changeset.data._id)}, %{"$set": Map.delete(changeset.changes, :_id)}) do
      {:ok, %{}} -> {:ok, struct(changeset.data, changeset.changes)}
      {:ok, nil} -> {:error, :not_found}
    end
  end

  def update(%{valid?: false} = changeset) do
    {:error, changeset}
  end

  defp parse_id(%BSON.ObjectId{} = id) do
    id
  end

  defp parse_id(id) when is_binary(id) do
    BSON.ObjectId.decode!(id)
  end

  defp convert_map_keys_to_atoms(some_map) do
    Enum.map(some_map, fn {key, value} -> {String.to_atom(key), value} end)
  end

  defp add_timestamp_changes(changeset) do
    current_datetime = DateTime.now!("Etc/UTC")
    Ecto.Changeset.change(
      changeset,
      %{inserted_at: changeset.data.inserted_at || current_datetime, updated_at: current_datetime}
    )
  end
end
