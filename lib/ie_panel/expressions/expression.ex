defmodule IEPanel.Expressions.Expression do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:_id, :string, []}

  schema "expressions" do
    field :name, :string
    field :verb, :string
    field :image_url, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(expression, attrs) do
    expression
    |> cast(attrs, [:name, :verb, :image_url])
    |> validate_required([:name, :verb, :image_url])
    |> validate_format(:image_url, ~r"^https?://.+\..+\.gif")
  end
end

defimpl Phoenix.Param, for: IEPanel.Expressions.Expression do
  def to_param(%{_id: %BSON.ObjectId{} = id}), do: BSON.ObjectId.encode!(id)

  def to_param(%{_id: id}) when is_binary(id), do: id
end

defimpl Phoenix.HTML.Safe, for: BSON.ObjectId do
  def to_iodata(id) do
    BSON.ObjectId.encode!(id)
  end
end
