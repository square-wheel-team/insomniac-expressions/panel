defmodule IEPanelWeb.Router do
  use IEPanelWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {IEPanelWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_user_in_conn
  end

  defp put_user_in_conn(conn, _opts) do
    conn
    |> Plug.Conn.assign(:discord_id, get_session(conn, :discord_id))
    |> Plug.Conn.assign(:username, get_session(conn, :username))
    |> Plug.Conn.assign(:avatar, get_session(conn, :avatar))
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :admin do
    plug :ensure_admin
  end

  defp ensure_admin(conn, _opts) do
    case Mongo.find_one(:mongo, "settings", %{}) do
      %{} = settings ->
        if !Enum.member?(settings["admins"], get_session(conn, :discord_id)) do
          conn |> send_resp(401, "Unauthorized") |> halt()
        else
          conn
        end
      nil -> conn |> send_resp(401, "Unauthorized") |> halt()
    end
  end

  scope "/", IEPanelWeb do
    pipe_through :browser

    get "/", PageController, :home
  end

  scope "/", IEPanelWeb do
    post "/discord", DiscordController, :send_message
  end

  scope "/", IEPanelWeb do
    pipe_through [:browser, :admin]

    resources "/expressions", ExpressionController
  end

  scope "/oauth", IEPanelWeb do
    pipe_through :browser

    get "/login", DiscordOAuthController, :start_discord_oauth
    get "/authorise", DiscordOAuthController, :finish_discord_oauth
    get "/logout", DiscordOAuthController, :logout
  end

  # Other scopes may use custom stacks.
  # scope "/api", IEPanelWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard in development
  if Application.compile_env(:ie_panel, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: IEPanelWeb.Telemetry
    end
  end
end
