defmodule IEPanelWeb.PageController do
  use IEPanelWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    render(
      conn,
      :home,
      layout: false,
      username: get_session(conn, :username),
      discord_id: get_session(conn, :discord_id),
      avatar: get_session(conn, :avatar),
      body_bg_class: "bg-logo"
    )
  end
end
