defmodule IEPanelWeb.DiscordOAuthController do
  use IEPanelWeb, :controller

  @discord_base_url "https://discord.com"
  @discord_oauth_url "#{@discord_base_url}/api/oauth2"
  @discord_token_url "#{@discord_oauth_url}/token"
  @response_type "code"
  @scope "identify"
  @grant_type "authorization_code"
  @urlencoded_headers [{"Content-Type", "application/x-www-form-urlencoded"}]

  defp client_id do
    Application.get_env(:ie_panel, :discord_oauth)[:client_id]
  end

  defp client_secret do
    Application.get_env(:ie_panel, :discord_oauth)[:client_secret]
  end

  defp redirect_uri do
    url(~p"/oauth/authorise")
  end

  def start_discord_oauth(conn, _params) do
    redirect(conn, external: "#{@discord_base_url}/oauth2/authorize/?response_type=#{@response_type}&client_id=#{client_id()}&scope=#{@scope}&redirect_uri=#{redirect_uri()}")
  end

  def finish_discord_oauth(conn, %{"code" => code} = _params) do
    case exchange_code_for_token(code) do
      {:ok, access_token} ->
        %{"user" => %{"id" => id, "username" => username, "avatar" => avatar}} = get_discord_user(access_token)
        revoke_token(access_token)
        conn
        |> put_session(:discord_id, id)
        |> put_session(:username, username)
        |> put_session(:avatar, avatar)
        |> redirect(to: ~p"/")
      {:error, %{reason: reason}} -> render(conn, :home, error: reason)
    end
  end

  defp exchange_code_for_token(code) do
    case HTTPoison.post(
      "#{@discord_token_url}",
      "grant_type=#{@grant_type}&code=#{code}&redirect_uri=#{redirect_uri()}",
      @urlencoded_headers,
      hackney: [basic_auth: {client_id(), client_secret()}]
    ) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        %{"access_token" => access_token} = Jason.decode!(body)
        {:ok, access_token}
      {:ok, %HTTPoison.Response{status_code: 401}} -> {:error, %{reason: "Unauthorized"}}
      {:ok, %HTTPoison.Response{status_code: 400}} -> {:error, %{reason: "Bad Request"}}
      {:error, %HTTPoison.Error{reason: reason}} -> {:error, %{reason: reason}}
    end
  end

  defp revoke_token(token) do
    HTTPoison.post!("#{@discord_token_url}/revoke", "token=#{token}", @urlencoded_headers)
  end

  defp get_discord_user(access_token) do
    HTTPoison.get!("#{@discord_oauth_url}/@me", [{"Authorization", "Bearer #{access_token}"}]).body
    |> Jason.decode!()
  end

  def logout(conn, _params) do
    conn
    |> delete_session(:username)
    |> delete_session(:discord_id)
    |> delete_session(:avatar)
    |> redirect(to: ~p"/")
  end
end
