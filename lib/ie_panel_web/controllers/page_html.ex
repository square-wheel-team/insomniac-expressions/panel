defmodule IEPanelWeb.PageHTML do
  use IEPanelWeb, :html

  embed_templates "page_html/*"
end
