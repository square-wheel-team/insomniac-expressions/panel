defmodule IEPanelWeb.DiscordHTML do
  use IEPanelWeb, :html

  embed_templates "discord_html/*"
end
