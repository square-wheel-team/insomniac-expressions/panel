defmodule IEPanelWeb.ExpressionHTML do
  use IEPanelWeb, :html

  embed_templates "expression_html/*"

  @doc """
  Renders a expression form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true

  def expression_form(assigns)
end
