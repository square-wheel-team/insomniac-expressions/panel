defmodule IEPanelWeb.ExpressionController do
  use IEPanelWeb, :controller

  alias IEPanel.Expressions
  alias IEPanel.Expressions.Expression

  def index(conn, _params) do
    expressions = Expressions.list_expressions()
    render(conn, :index, expressions: expressions)
  end

  def new(conn, _params) do
    changeset = Expressions.change_expression(%Expression{})
    render(conn, :new, changeset: changeset, scripts: ["expression_form.js"])
  end

  def create(conn, %{"expression" => expression_params}) do
    case Expressions.create_expression(expression_params) do
      {:ok, expression} ->
        conn
        |> put_flash(:info, "Expression created successfully.")
        |> redirect(to: ~p"/expressions/#{expression}")

      {:error, %Ecto.Changeset{} = changeset} ->
        IO.puts(inspect changeset)
        render(conn, :new, changeset: %{changeset | action: :insert})
    end
  end

  def show(conn, %{"id" => id}) do
    expression = Expressions.get_expression!(id)
    render(conn, :show, expression: expression)
  end

  def edit(conn, %{"id" => id}) do
    expression = Expressions.get_expression!(id)
    changeset = Expressions.change_expression(expression)
    render(conn, :edit, expression: expression, changeset: changeset, scripts: ["expression_form.js"])
  end

  def update(conn, %{"id" => id, "expression" => expression_params}) do
    expression = Expressions.get_expression!(id)

    case Expressions.update_expression(expression, expression_params) do
      {:ok, expression} ->
        conn
        |> put_flash(:info, "Expression updated successfully.")
        |> redirect(to: ~p"/expressions/#{expression}")

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, expression: expression, changeset: %{changeset | action: :update})
    end
  end

  def delete(conn, %{"id" => id}) do
    expression = Expressions.get_expression!(id)
    {:ok, _expression} = Expressions.delete_expression(expression)

    conn
    |> put_flash(:info, "Expression deleted successfully.")
    |> redirect(to: ~p"/expressions")
  end
end
