defmodule IEPanelWeb.Layouts do
  use IEPanelWeb, :html

  embed_templates "layouts/*"
end
