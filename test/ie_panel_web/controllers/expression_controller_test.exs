defmodule IEPanelWeb.ExpressionControllerTest do
  use IEPanelWeb.ConnCase

  import IEPanel.ExpressionsFixtures

  @create_attrs %{name: "some name", verb: "some verb", image_url: "some image_url"}
  @update_attrs %{name: "some updated name", verb: "some updated verb", image_url: "some updated image_url"}
  @invalid_attrs %{name: nil, verb: nil, image_url: nil}

  describe "index" do
    test "lists all expressions", %{conn: conn} do
      conn = get(conn, ~p"/expressions")
      assert html_response(conn, 200) =~ "Listing Expressions"
    end
  end

  describe "new expression" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/expressions/new")
      assert html_response(conn, 200) =~ "New Expression"
    end
  end

  describe "create expression" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/expressions", expression: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/expressions/#{id}"

      conn = get(conn, ~p"/expressions/#{id}")
      assert html_response(conn, 200) =~ "Expression #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/expressions", expression: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Expression"
    end
  end

  describe "edit expression" do
    setup [:create_expression]

    test "renders form for editing chosen expression", %{conn: conn, expression: expression} do
      conn = get(conn, ~p"/expressions/#{expression}/edit")
      assert html_response(conn, 200) =~ "Edit Expression"
    end
  end

  describe "update expression" do
    setup [:create_expression]

    test "redirects when data is valid", %{conn: conn, expression: expression} do
      conn = put(conn, ~p"/expressions/#{expression}", expression: @update_attrs)
      assert redirected_to(conn) == ~p"/expressions/#{expression}"

      conn = get(conn, ~p"/expressions/#{expression}")
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, expression: expression} do
      conn = put(conn, ~p"/expressions/#{expression}", expression: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Expression"
    end
  end

  describe "delete expression" do
    setup [:create_expression]

    test "deletes chosen expression", %{conn: conn, expression: expression} do
      conn = delete(conn, ~p"/expressions/#{expression}")
      assert redirected_to(conn) == ~p"/expressions"

      assert_error_sent 404, fn ->
        get(conn, ~p"/expressions/#{expression}")
      end
    end
  end

  defp create_expression(_) do
    expression = expression_fixture()
    %{expression: expression}
  end
end
