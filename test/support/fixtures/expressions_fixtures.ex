defmodule IEPanel.ExpressionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `IEPanel.Expressions` context.
  """

  @doc """
  Generate a expression.
  """
  def expression_fixture(attrs \\ %{}) do
    {:ok, expression} =
      attrs
      |> Enum.into(%{
        image_url: "some image_url",
        name: "some name",
        verb: "some verb"
      })
      |> IEPanel.Expressions.create_expression()

    expression
  end
end
