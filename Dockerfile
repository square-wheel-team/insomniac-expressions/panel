FROM elixir:alpine

RUN apk add inotify-tools

WORKDIR /app
COPY . /app
COPY .env .
RUN mix deps.get
EXPOSE 4000

RUN echo "MONGO_HOST=mongo" >> "./.env"

CMD ["mix", "phx.server"]
